﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Job : AuditableEntity
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }
        public string Company { get; set; }

        public string Vacancy { get; set; }

        public int NumberOfUnits { get; set; }

        public decimal Salary { get; set; }

        public string JobRequiremets { get; set; }

        public string EmployerPhone { get; set; }

        public string EmployerEmail { get; set; }

        public string Industry { get; set; }
        public string Notes { get; set; }
        public string EmployerAddress { get; set; }
        
        public ICollection<Job> Jobs { get; set; }
    }
}
