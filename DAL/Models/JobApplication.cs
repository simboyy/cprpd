﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Linq;

namespace DAL.Models
{
    public class JobApplication : AuditableEntity
    {
        public int Id { get; set; }
      

        public int PatientId { get; set; }
        public Patient Patient { get; set; }

        public int ApplicationId { get; set; }
        public JobApplication Application { get; set; }

        public string Status { get; set; }

        public string Notes { get; set; }
    }
}
