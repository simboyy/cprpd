﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Patient : AuditableEntity
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string PlaceOfResidence { get; set; }
        public int DisabilityGroup { get; set; }
        public string Disability { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string HearingSpeechDiction { get; set; }
        public string Education { get; set; }
        public string Training { get; set; }
        public string Motivation { get; set; }
        public string DesiredWorkingConditions { get; set; }
        public string DesiredVacancy { get; set; }
        public string EmploymentStatus { get; set; }
        public string FormOfEmployment { get; set; }
        public string EmploymentDate { get; set; }
        public string Availability { get; set; }
        public DateTime DeadLineForSupport { get; set; }
        public string AdditionalInfo { get; set; }
        public string ResponsiblePerson { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }


        public ICollection<DisabilityGroup> DisabilityGroups { get; set; }
    }
}
