﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Linq;

namespace DAL.Models
{
    public class RehabilitationDetail : AuditableEntity
    {
        public int Id { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }


        public int AppointmentId { get; set; }
        public Appointment Appointment { get; set; }

        public int RehabilitationId { get; set; }
        public Rehabilitation Rehabilitation { get; set; }
    }
}
