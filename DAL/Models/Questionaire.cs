﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Questionaire : AuditableEntity
    {
        public int Id { get; set; }
        public decimal Discount { get; set; }
        public string Comments { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }


        public string CashierId { get; set; }
        public ApplicationUser Cashier { get; set; }

        public int PatientId { get; set; }
        public Patient Patient { get; set; }


        public ICollection<Questionaire> Questionaires { get; set; }
    }
}
