﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories;
using DAL.Repositories.Interfaces;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        IPatientRepository _Patients;
        IAppointmentRepository _appointments;
        IRehabilitationRepository _rehabilitations;
        IRehabilitationPlanRepository _rehabilitationPlans;
        IJobRepository _jobs;
        IJobApplicationRepository _jobApplications;
        IQuestionaireRepository _Questionaires;
        ICategoryRepository _Categories;
        IGenderRepository _Genders;
        IDisabilityGroupRepository _DisabilityGroups;





        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }



        public IPatientRepository Patients
        {
            get
            {
                if (_Patients == null)
                    _Patients = new PatientRepository(_context);

                return _Patients;
            }
        }



        public IAppointmentRepository Appointments
        {
            get
            {
                if (_appointments == null)
                    _appointments = new AppointmentRepository(_context);

                return _appointments;
            }
        }



        public IRehabilitationRepository Rehabilitations
        {
            get
            {
                if (_rehabilitations == null)
                    _rehabilitations = new RehabilitationRepository(_context);

                return _rehabilitations;
            }
        }


        public IQuestionaireRepository Questionaires
        {
            get
            {
                if (_Questionaires == null)
                    _Questionaires = new QuestionaireRepository(_context);

                return _Questionaires;
            }
        }
      
      

       

        public IJobRepository Jobs
        {
            get
            {
                if (_jobs == null)
                    _jobs = new JobRepository(_context);

                return _jobs;
            }
        }


        public IRehabilitationPlanRepository RehabilitationPlans
        {
            get
            {
                if (_rehabilitationPlans == null)
                    _rehabilitationPlans = new RehabilitationPlanRepository(_context);

                return _rehabilitationPlans;
            }
        }


        public ICategoryRepository Categories
        {
            get
            {
                if (_Categories == null)
                    _Categories = new CategoryRepository(_context);

                return _Categories;
            }
        }

        public IGenderRepository Genders
        {
            get
            {
                if (_Genders == null)
                    _Genders = new GenderRepository(_context);

                return Genders;
            }
        }



        public IDisabilityGroupRepository DisabilityGroups
        {
            get
            {
                if (_DisabilityGroups == null)
                    _DisabilityGroups = new DisabilityGroupRepository(_context);

                return _DisabilityGroups;
            }
        }





        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
