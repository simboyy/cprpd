﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        IPatientRepository Patients { get; }
        IAppointmentRepository Appointments { get; }
        IRehabilitationRepository Rehabilitations { get; }
        IQuestionaireRepository Questionaires { get; }
        IRehabilitationPlanRepository RehabilitationPlans { get; }
        IJobRepository Jobs { get; }
        ICategoryRepository Categories { get; }
        IGenderRepository Genders { get; }
        IDisabilityGroupRepository DisabilityGroups { get; }
        int SaveChanges();
    }
}
