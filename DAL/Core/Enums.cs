﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;

namespace DAL.Core
{
    public enum Sex
    {
        None,
        Female,
        Male
    }
}
