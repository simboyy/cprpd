﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class JobApplicationRepository : Repository<JobApplication>, IJobApplicationRepository
    {
        public JobApplicationRepository(ApplicationDbContext context) : base(context)
        { }


        public IEnumerable<JobApplication> GetTopActiveJobApplications(int count)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<JobApplication> GetAllJobApplicationsData()
        {
            return _appContext.JobApplications
                .OrderBy(c => c.ApplicationId)
                .ToList();
        }



        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;
    }
}
