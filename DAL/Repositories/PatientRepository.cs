﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class PatientRepository : Repository<Patient>, IPatientRepository
    {
        public PatientRepository(ApplicationDbContext context) : base(context)
        { }


        public IEnumerable<Patient> GetTopActivePatients(int count)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Patient> GetAllPatientsData()
        {
            return _appContext.Patients
               // .Include(c => c.DisabilityGroups).ThenInclude(o => o.RehabilitationDetails).ThenInclude(d => d.Appointment)
               // .Include(c => c.DisabilityGroups).ThenInclude(o => o.Cashier)
                .OrderBy(c => c.FullName)
                .ToList();
        }



        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;
    }
}
