﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories.Interfaces
{
    public interface IJobRepository : IRepository<Job>
    {
        IEnumerable<Job> GetTopActiveJobs(int count);
        IEnumerable<Job> GetAllJobsData();
    }
}
