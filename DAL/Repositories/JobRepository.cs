﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class JobRepository : Repository<Job>, IJobRepository
    {
        public JobRepository(ApplicationDbContext context) : base(context)
        { }


        public IEnumerable<Job> GetTopActiveJobs(int count)
        {
            throw new NotImplementedException();
        }


        public IEnumerable<Job> GetAllJobsData()
        {
            return _appContext.Jobs
                .OrderBy(c => c.Vacancy)
                .ToList();
        }



        private ApplicationDbContext _appContext => (ApplicationDbContext)_context;
    }
}
