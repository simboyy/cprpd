﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DAL.Models.Interfaces;

namespace DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public string CurrentUserId { get; set; }
        public DbSet<Patient> Patients { get; set; }

        public DbSet<Job> Jobs { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<DisabilityGroup> DisabilityGroups { get; set; }

        public DbSet<Gender> Genders { get; set; }

        public DbSet<JobApplication> JobApplications { get; set; }
        public DbSet<AppointmentCategory> AppointmentCategories { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Rehabilitation> Rehabilitations { get; set; }
        public DbSet<RehabilitationDetail> RehabilitationDetails { get; set; }

        public DbSet<RehabilitationPlan> RehabilitationPlans { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        { }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            const string priceDecimalType = "decimal(18,2)";

            builder.Entity<ApplicationUser>().HasMany(u => u.Claims).WithOne().HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasMany(u => u.Roles).WithOne().HasForeignKey(r => r.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ApplicationRole>().HasMany(r => r.Claims).WithOne().HasForeignKey(c => c.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationRole>().HasMany(r => r.Users).WithOne().HasForeignKey(r => r.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Patient>().Property(c => c.FullName).IsRequired().HasMaxLength(100);
            builder.Entity<Patient>().HasIndex(c => c.FullName);
            builder.Entity<Patient>().Property(c => c.EmailAddress).HasMaxLength(100);
            builder.Entity<Patient>().Property(c => c.PhoneNumber).IsUnicode(false).HasMaxLength(30);
          //  builder.Entity<Patient>().Property(c => c.City).HasMaxLength(50);
            builder.Entity<Patient>().ToTable($"App{nameof(this.Patients)}");


            builder.Entity<Job>().Property(c => c.Company).IsRequired().HasMaxLength(100);
            builder.Entity<Job>().Property(c => c.Vacancy);
            builder.Entity<Job>().Property(c => c.Salary).HasMaxLength(100);
            builder.Entity<Job>().Property(c => c.NumberOfUnits).IsUnicode(false).HasMaxLength(30);
            builder.Entity<Job>().Property(c => c.EmployerAddress);
            builder.Entity<Job>().Property(c => c.EmployerEmail);
            builder.Entity<Job>().Property(c => c.EmployerPhone);
            builder.Entity<Job>().Property(c => c.Notes);
            builder.Entity<Job>().ToTable($"App{nameof(this.Jobs)}");


           
            builder.Entity<JobApplication>().Property(c => c.Notes);       
            builder.Entity<JobApplication>().ToTable($"App{nameof(this.JobApplications)}");

            builder.Entity<AppointmentCategory>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.Entity<AppointmentCategory>().Property(p => p.Description).HasMaxLength(500);
            builder.Entity<AppointmentCategory>().ToTable($"App{nameof(this.AppointmentCategories)}");

            builder.Entity<Gender>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.Entity<Gender>().Property(p => p.Description).HasMaxLength(500);
            builder.Entity<Gender>().ToTable($"App{nameof(this.Genders)}");

            builder.Entity<Category>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.Entity<Category>().Property(p => p.Description).HasMaxLength(500);
            builder.Entity<Category>().ToTable($"App{nameof(this.Categories)}");

            builder.Entity<DisabilityGroup>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.Entity<DisabilityGroup>().Property(p => p.Description).HasMaxLength(500);
            builder.Entity<DisabilityGroup>().ToTable($"App{nameof(this.DisabilityGroups)}");

            builder.Entity<Appointment>().Property(p => p.Name).IsRequired().HasMaxLength(100);
            builder.Entity<Appointment>().HasIndex(p => p.Name);
            builder.Entity<Appointment>().Property(p => p.Description).HasMaxLength(500);
            builder.Entity<Appointment>().Property(p => p.Icon).IsUnicode(false).HasMaxLength(256);
            builder.Entity<Appointment>().HasOne(p => p.Parent).WithMany(p => p.Children).OnDelete(DeleteBehavior.Restrict);
            builder.Entity<Appointment>().ToTable($"App{nameof(this.Appointments)}");
            builder.Entity<Appointment>().Property(p => p.BuyingPrice).HasColumnType(priceDecimalType);
            builder.Entity<Appointment>().Property(p => p.SellingPrice).HasColumnType(priceDecimalType);

            builder.Entity<Rehabilitation>().Property(o => o.Comments).HasMaxLength(500);
            builder.Entity<Rehabilitation>().ToTable($"App{nameof(this.Rehabilitations)}");
            builder.Entity<Rehabilitation>().Property(p => p.Discount).HasColumnType(priceDecimalType);

            builder.Entity<RehabilitationDetail>().ToTable($"App{nameof(this.RehabilitationDetails)}");
            builder.Entity<RehabilitationDetail>().Property(p => p.UnitPrice).HasColumnType(priceDecimalType);
            builder.Entity<RehabilitationDetail>().Property(p => p.Discount).HasColumnType(priceDecimalType);
        }




        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(cancellationToken);
        }


        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        private void UpdateAuditEntities()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var entry in modifiedEntries)
            {
                var entity = (IAuditableEntity)entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                    entity.CreatedBy = CurrentUserId;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.UpdatedDate = now;
                entity.UpdatedBy = CurrentUserId;
            }
        }
    }
}
