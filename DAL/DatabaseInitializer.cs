﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Core;
using DAL.Core.Interfaces;

namespace DAL
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }




    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly IAccountManager _accountManager;
        private readonly ILogger _logger;

        public DatabaseInitializer(ApplicationDbContext context, IAccountManager accountManager, ILogger<DatabaseInitializer> logger)
        {
            _accountManager = accountManager;
            _context = context;
            _logger = logger;
        }

        public async Task SeedAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (!await _context.Users.AnyAsync())
            {
                _logger.LogInformation("Generating inbuilt accounts");

                const string adminRoleName = "administrator";
                const string userRoleName = "user";

                await EnsureRoleAsync(adminRoleName, "Default administrator", ApplicationPermissions.GetAllPermissionValues());
                await EnsureRoleAsync(userRoleName, "Default user", new string[] { });

                await CreateUserAsync("admin", "tempP@ss123", "Inbuilt Administrator", "admin@ebenmonney.com", "+1 (123) 000-0000", new string[] { adminRoleName });
                await CreateUserAsync("user", "tempP@ss123", "Inbuilt Standard User", "user@ebenmonney.com", "+1 (123) 000-0001", new string[] { userRoleName });

                _logger.LogInformation("Inbuilt account generation completed");
            }



            if (!await _context.Patients.AnyAsync() && !await _context.AppointmentCategories.AnyAsync())
            {
                _logger.LogInformation("Seeding initial data");

                Patient cust_1 = new Patient
                {
                    FullName = "Ebenezer Monney",
                    EmailAddress = "contact@ebenmonney.com",
                    Gender = "Male",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };

                Patient cust_2 = new Patient
                {
                    FullName = "Itachi Uchiha",
                    EmailAddress = "uchiha@narutoverse.com",
                    PhoneNumber = "+81123456789",
                    PlaceOfResidence= "Some fictional Address, Street 123, Kono",
                    Gender = "Male",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };

                Patient cust_3 = new Patient
                {
                    FullName = "John Doe",
                    EmailAddress = "johndoe@anonymous.com",
                    PhoneNumber = "+18585858",
                    PlaceOfResidence = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio.
                    Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet",
                    Gender = "Male",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };

                Patient cust_4 = new Patient
                {
                    FullName = "Jane Doe",
                    EmailAddress = "Janedoe@anonymous.com",
                    PhoneNumber = "+18585858",
                    PlaceOfResidence = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio.
                    Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet",
                    Gender = "Male",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };



                AppointmentCategory prodCat_1 = new AppointmentCategory
                {
                    Name = "None",
                    Description = "Default category. Appointments that have not been assigned a category",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };



                Appointment prod_1 = new Appointment
                {
                    Name = "BMW M6",
                    Description = "Yet another masterpiece from the world's best car manufacturer",
                    BuyingPrice = 109775,
                    SellingPrice = 114234,
                    UnitsInStock = 12,
                    IsActive = true,
                    AppointmentCategory = prodCat_1,
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };

                Appointment prod_2 = new Appointment
                {
                    Name = "Nissan Patrol",
                    Description = "A true man's choice",
                    BuyingPrice = 78990,
                    SellingPrice = 86990,
                    UnitsInStock = 4,
                    IsActive = true,
                    AppointmentCategory = prodCat_1,
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };



                Rehabilitation ordr_1 = new Rehabilitation
                {
                    Discount = 500,
                    Cashier = await _context.Users.FirstAsync(),
                    Patient = cust_1,
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow,
                    RehabilitationDetails = new List<RehabilitationDetail>()
                    {
                        new RehabilitationDetail() {UnitPrice = prod_1.SellingPrice, Quantity=1, Appointment = prod_1 },
                        new RehabilitationDetail() {UnitPrice = prod_2.SellingPrice, Quantity=1, Appointment = prod_2 },
                    }
                };

                Rehabilitation ordr_2 = new Rehabilitation
                {
                    Cashier = await _context.Users.FirstAsync(),
                    Patient = cust_2,
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow,
                    RehabilitationDetails = new List<RehabilitationDetail>()
                    {
                        new RehabilitationDetail() {UnitPrice = prod_2.SellingPrice, Quantity=1, Appointment = prod_2 },
                    }
                };


                
                Gender gdr1 = new Gender
                {
                    Name = "Male",
                    Description = ""     
                };

                 Gender gdr2 = new Gender
                {
                    Name = "Female",
                    Description = ""     
                };


                 DisabilityGroup dsb1 = new DisabilityGroup
                {
                    Name = "Group 1",
                    Description = ""     
                };

                DisabilityGroup dsb2 = new DisabilityGroup
                {
                    Name = "Group 2",
                    Description = ""     
                };


                  Category cat1 = new Category
                {
                    Name = "Employment and Support",
                    Description = ""     
                };

                Category ca2 = new Category
                {
                    Name = "Employment and the end of the support period",
                    Description = ""     
                };


                 Category ca3 = new Category
                {
                    Name = "Refusal of Employment and the end of the escort period",
                    Description = ""     
                };


                  Category ca4 = new Category
                {
                    Name = "looking for work",
                    Description = ""     
                };





               _context.Genders.Add(gdr1);
               _context.Genders.Add(gdr2);

                _context.DisabilityGroups.Add(dsb1);
                _context.DisabilityGroups.Add(dsb2);

                _context.Categories.Add(cat1);
                _context.Categories.Add(ca2);
                _context.Categories.Add(ca3);
                _context.Categories.Add(ca4);


                _context.Patients.Add(cust_1);
                _context.Patients.Add(cust_2);
                _context.Patients.Add(cust_3);
                _context.Patients.Add(cust_4);

              //  _context.Appointments.Add(prod_1);
               // _context.Appointments.Add(prod_2);

               // _context.Rehabilitations.Add(ordr_1);
                //.Rehabilitations.Add(ordr_2);

                await _context.SaveChangesAsync();

                _logger.LogInformation("Seeding initial data completed");
            }
        }



        private async Task EnsureRoleAsync(string roleName, string description, string[] claims)
        {
            if ((await _accountManager.GetRoleByNameAsync(roleName)) == null)
            {
                ApplicationRole applicationRole = new ApplicationRole(roleName, description);

                var result = await this._accountManager.CreateRoleAsync(applicationRole, claims);

                if (!result.Succeeded)
                    throw new Exception($"Seeding \"{description}\" role failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");
            }
        }

        private async Task<ApplicationUser> CreateUserAsync(string userName, string password, string fullName, string email, string phoneNumber, string[] roles)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserName = userName,
                FullName = fullName,
                Email = email,
                PhoneNumber = phoneNumber,
                EmailConfirmed = true,
                IsEnabled = true
            };

            var result = await _accountManager.CreateUserAsync(applicationUser, roles, password);

            if (!result.Succeeded)
                throw new Exception($"Seeding \"{userName}\" user failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");


            return applicationUser;
        }
    }
}
