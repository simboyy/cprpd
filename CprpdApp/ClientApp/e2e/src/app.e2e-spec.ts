// =============================
// Email: smkorera@gmail.com

// =============================

import { AppPage } from './app.po';

describe('CprpdApp App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display application title: CprpdApp', async () => {
    await page.navigateTo();
    expect(await page.getAppTitle()).toEqual('CprpdApp');
  });
});
