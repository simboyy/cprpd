// =============================
// Email: smkorera@gmail.com

// =============================

import { Injectable } from '@angular/core';
import { Observable, Subject, forkJoin } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';

import { PatientEndpoint } from './patient-endpoint.service';
import { AuthService } from './auth.service';
import { Patient } from '../models/patient-model';
import { Role } from '../models/role.model';
import { Permission, PermissionValues } from '../models/permission.model';
import { PatientEdit } from '../models/patient-edit.model';

export type RolesChangedOperation = 'add' | 'delete' | 'modify';
export interface RolesChangedEventArg { roles: Role[] | string[]; operation: RolesChangedOperation; }

@Injectable()
export class PatientService {
  public static readonly roleAddedOperation: RolesChangedOperation = 'add';
  public static readonly roleDeletedOperation: RolesChangedOperation = 'delete';
  public static readonly roleModifiedOperation: RolesChangedOperation = 'modify';

  private rolesChanged = new Subject<RolesChangedEventArg>();

  constructor(
    private authService: AuthService,
    private patient: PatientEndpoint) {

  }

  getPatient(userId?: string) {
    return this.patient.getPatientEndpoint<Patient>(userId);
  }

  getPatientAndRoles(userId?: string) {

      this.patient.getPatientEndpoint<Patient>(userId)
     
  }

  getPatients(page?: number, pageSize?: number) {

    return this.patient.getPatientsEndpoint<Patient[]>(page, pageSize);
  }

  getPatientsAndRoles(page?: number, pageSize?: number) {

    return this.patient.getPatientsEndpoint<Patient[]>(page, pageSize)
  }

  updatePatient(user: PatientEdit) {
    if (user.id) {
      return this.patient.getUpdatePatientEndpoint(user, user.id);
    } else {
      return this.patient.getPatientByPatientNameEndpoint<Patient>(user.fullName).pipe(
        mergeMap(foundPatient => {
          user.id = foundPatient.id;
          return this.patient.getUpdatePatientEndpoint(user, user.id);
        }));
    }
  }



  newPatient(patient: PatientEdit) {
    return this.patient.getNewPatientEndpoint<Patient>(patient);
  }

  

  deletePatient(userOrPatientId: string | Patient): Observable<Patient> {
    if (typeof userOrPatientId === 'string' || userOrPatientId instanceof String) {
      return this.patient.getDeletePatientEndpoint<Patient>(userOrPatientId as string).pipe<Patient>(
        tap(data => {} )
        
        );
    } else {
      if (userOrPatientId.id) {
        return this.deletePatient(userOrPatientId.id);
      } else {
        return this.patient.getPatientByPatientNameEndpoint<Patient>(userOrPatientId.fullName).pipe<Patient>(
          mergeMap(user => this.deletePatient(user.id)));
      }
    }
  }

  
  userHasPermission(permissionValue: PermissionValues): boolean {
    return this.permissions.some(p => p === permissionValue);
  }

  refreshLoggedInPatient() {
    return this.patient.refreshLogin();
  }

  
  

  

  getPermissions() {

    return this.patient.getPermissionsEndpoint<Permission[]>();
  }

  private onRolesChanged(roles: Role[] | string[], op: RolesChangedOperation) {
    this.rolesChanged.next({ roles, operation: op });
  }

  

  getRolesChangedEvent(): Observable<RolesChangedEventArg> {
    return this.rolesChanged.asObservable();
  }

  get permissions(): PermissionValues[] {
    return this.authService.userPermissions;
  }

  get currentPatient() {
    return this.authService.currentUser;
  }
}
