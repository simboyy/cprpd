// =============================
// Email: smkorera@gmail.com

// =============================

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { EndpointBase } from './endpoint-base.service';
import { ConfigurationService } from './configuration.service';


@Injectable()
export class PatientEndpoint extends EndpointBase {

  get patientUrl() { return this.configurations.baseUrl + '/api/patient'; }
  get patientByPatientNameUrl() { return this.configurations.baseUrl + '/api/patient/patientname'; }
  get currentPatientUrl() { return this.configurations.baseUrl + '/api/patient/me'; }
  get currentPatientPreferencesUrl() { return this.configurations.baseUrl + '/api/patient/me/preferences'; }
  get unblockPatientUrl() { return this.configurations.baseUrl + '/api/patient/unblock'; }
  get rolesUrl() { return this.configurations.baseUrl + '/api/roles'; }
  get roleByRoleNameUrl() { return this.configurations.baseUrl + '/api/roles/name'; }
  get permissionsUrl() { return this.configurations.baseUrl + '/api/permissions'; }


  constructor(private configurations: ConfigurationService, http: HttpClient, authService: AuthService) {
    super(http, authService);
  }


  getPatientEndpoint<T>(patientId?: string): Observable<T> {
    const endpointUrl = patientId ? `${this.patientUrl}/${patientId}` : this.currentPatientUrl;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getPatientEndpoint(patientId));
      }));
  }


  getPatientByPatientNameEndpoint<T>(patientName: string): Observable<T> {
    const endpointUrl = `${this.patientByPatientNameUrl}/${patientName}`;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getPatientByPatientNameEndpoint(patientName));
      }));
  }


  getPatientsEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
    const endpointUrl = page && pageSize ? `${this.patientUrl}/${page}/${pageSize}` : this.patientUrl;

    return this.http.get<T>(endpointUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getPatientsEndpoint(page, pageSize));
      }));
  }


  getNewPatientEndpoint<T>(patientObject: any): Observable<T> {

    return this.http.post<T>(this.patientUrl, JSON.stringify(patientObject), this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getNewPatientEndpoint(patientObject));
      }));
  }

  getUpdatePatientEndpoint<T>(patientObject: any, patientId?: string): Observable<T> {
    const endpointUrl = patientId ? `${this.patientUrl}/${patientId}` : this.currentPatientUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(patientObject), this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getUpdatePatientEndpoint(patientObject, patientId));
      }));
  }

  getPatchUpdatePatientEndpoint<T>(patch: {}, patientId?: string): Observable<T>;
  getPatchUpdatePatientEndpoint<T>(value: any, op: string, path: string, from?: any, patientId?: string): Observable<T>;
  getPatchUpdatePatientEndpoint<T>(valueOrPatch: any, opOrPatientId?: string, path?: string, from?: any, patientId?: string): Observable<T> {
    let endpointUrl: string;
    let patchDocument: {};

    if (path) {
      endpointUrl = patientId ? `${this.patientUrl}/${patientId}` : this.currentPatientUrl;
      patchDocument = from ?
        [{ value: valueOrPatch, path, op: opOrPatientId, from }] :
        [{ value: valueOrPatch, path, op: opOrPatientId }];
    } else {
      endpointUrl = opOrPatientId ? `${this.patientUrl}/${opOrPatientId}` : this.currentPatientUrl;
      patchDocument = valueOrPatch;
    }

    return this.http.patch<T>(endpointUrl, JSON.stringify(patchDocument), this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getPatchUpdatePatientEndpoint(valueOrPatch, opOrPatientId, path, from, patientId));
      }));
  }


 
  getDeletePatientEndpoint<T>(patientId: string): Observable<T> {
    const endpointUrl = `${this.patientUrl}/${patientId}`;

    return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getDeletePatientEndpoint(patientId));
      }));
  }


  getPermissionsEndpoint<T>(): Observable<T> {

    return this.http.get<T>(this.permissionsUrl, this.requestHeaders).pipe<T>(
      catchError(error => {
        return this.handleError(error, () => this.getPermissionsEndpoint());
      }));
  }



}
