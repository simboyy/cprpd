// =============================
// Email: smkorera@gmail.com

// =============================

import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animations';

@Component({
  selector: 'app-orders',
  templateUrl: './rehabilitations.component.html',
  styleUrls: ['./rehabilitations.component.scss'],
  animations: [fadeInOut]
})
export class RehabilitationsComponent {
}
