// =============================
// Email: smkorera@gmail.com

// =============================

import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { PatientService } from '../../services/patient.service';
import { Utilities } from '../../services/utilities';
import { Patient } from '../../models/patient-model';
import { PatientEdit } from '../../models/patient-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';


@Component({
  selector: 'app-patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.scss']
})
export class PatientInfoComponent implements OnInit {

  public isEditMode = false;
  public isNewPatient = false;
  public isSaving = false;
  public isChangePassword = false;
  public isEditingSelf = false;
  public showValidationErrors = false;
  public uniqueId: string = Utilities.uniqueId();
  public patient: Patient = new Patient();
  public patientEdit: PatientEdit;
  public allRoles: Role[] = [];

  public formResetToggle = true;

  public changesSavedCallback: () => void;
  public changesFailedCallback: () => void;
  public changesCancelledCallback: () => void;

  @Input()
  isViewOnly: boolean;

  @Input()
  isGeneralEditor = false;





  @ViewChild('f')
  public form;

  // ViewChilds Required because ngIf hides template variables from global scope
  @ViewChild('patientName')
  public patientName;

  @ViewChild('patientPassword')
  public patientPassword;

  @ViewChild('email')
  public email;

  @ViewChild('currentPassword')
  public currentPassword;

  @ViewChild('newPassword')
  public newPassword;

  @ViewChild('confirmPassword')
  public confirmPassword;

  @ViewChild('roles')
  public roles;


  constructor(private alertService: AlertService, private patientService: PatientService) {
  }

  ngOnInit() {
    if (!this.isGeneralEditor) {
      this.loadCurrentPatientData();
    }
  }



  private loadCurrentPatientData() {
    this.alertService.startLoadingMessage();

    if (this.canViewAllRoles) {
     // this.patientService.getPatientAndRoles().subscribe(results => this.onCurrentPatientDataLoadSuccessful(results[0], results[1]), error => this.onCurrentPatientDataLoadFailed(error));
    } else {
      this.patientService.getPatient().subscribe(patient => this.onCurrentPatientDataLoadSuccessful(patient, patient.disabilityGroups.map(x => new Role(x))), error => this.onCurrentPatientDataLoadFailed(error));
    }
  }


  private onCurrentPatientDataLoadSuccessful(patient: Patient, roles: Role[]) {
    this.alertService.stopLoadingMessage();
    this.patient = patient;
    this.allRoles = roles;
  }

  private onCurrentPatientDataLoadFailed(error: any) {
    this.alertService.stopLoadingMessage();
    this.alertService.showStickyMessage('Load Error', `Unable to retrieve patient data from the server.\r\nErrors: "${Utilities.getHttpResponseMessages(error)}"`,
      MessageSeverity.error, error);

    this.patient = new Patient();
  }



  getRoleByName(name: string) {
    return this.allRoles.find((r) => r.name === name);
  }



  showErrorAlert(caption: string, message: string) {
    this.alertService.showMessage(caption, message, MessageSeverity.error);
  }


  


  edit() {
    if (!this.isGeneralEditor) {
      this.isEditingSelf = true;
      this.patientEdit = new PatientEdit();
      Object.assign(this.patientEdit, this.patient);
    } else {
      if (!this.patientEdit) {
        this.patientEdit = new PatientEdit();
      }

      this.isEditingSelf = this.patientService.currentPatient ? this.patientEdit.id === this.patientService.currentPatient.id : false;
    }

    this.isEditMode = true;
    this.showValidationErrors = true;
    this.isChangePassword = false;
  }


  save() {
    this.isSaving = true;
    this.alertService.startLoadingMessage('Saving changes...');

    if (this.isNewPatient) {
      this.patientService.newPatient(this.patientEdit).subscribe(patient => this.saveSuccessHelper(patient), error => this.saveFailedHelper(error));
    } else {
      this.patientService.updatePatient(this.patientEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
    }
  }


  private saveSuccessHelper(patient?: Patient) {
    this.testIsRolePatientCountChanged(this.patient, this.patientEdit);

    if (patient) {
      Object.assign(this.patientEdit, patient);
    }

    this.isSaving = false;
    this.alertService.stopLoadingMessage();
    this.isChangePassword = false;
    this.showValidationErrors = false;

    
    Object.assign(this.patient, this.patientEdit);
    this.patientEdit = new PatientEdit();
    this.resetForm();


    if (this.isGeneralEditor) {
      if (this.isNewPatient) {
        this.alertService.showMessage('Success', `Patient \"${this.patient.fullName}\" was created successfully`, MessageSeverity.success);
      } else if (!this.isEditingSelf) {
        this.alertService.showMessage('Success', `Changes to patient \"${this.patient.fullName}\" was saved successfully`, MessageSeverity.success);
      }
    }

    if (this.isEditingSelf) {
      this.alertService.showMessage('Success', 'Changes to your Patient Profile was saved successfully', MessageSeverity.success);
      this.refreshLoggedInPatient();
    }

    this.isEditMode = false;


    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }


  private saveFailedHelper(error: any) {
    this.isSaving = false;
    this.alertService.stopLoadingMessage();
    this.alertService.showStickyMessage('Save Error', 'The below errors occured whilst saving your changes:', MessageSeverity.error, error);
    this.alertService.showStickyMessage(error, null, MessageSeverity.error);

    if (this.changesFailedCallback) {
      this.changesFailedCallback();
    }
  }



  private testIsRolePatientCountChanged(currentPatient: Patient, editedPatient: Patient) {

    // const rolesAdded = this.isNewPatient ? editedPatient.roles : editedPatient.roles.filter(role => currentPatient.roles.indexOf(role) === -1);
    // const rolesRemoved = this.isNewPatient ? [] : currentPatient.roles.filter(role => editedPatient.roles.indexOf(role) === -1);

    // const modifiedRoles = rolesAdded.concat(rolesRemoved);

    // if (modifiedRoles.length) {
    //   setTimeout(() => this.patientService.onRolesPatientCountChanged(modifiedRoles));
    // }
  }



  cancel() {
    if (this.isGeneralEditor) {
      this.patientEdit = this.patient = new PatientEdit();
    } else {
      this.patientEdit = new PatientEdit();
    }

    this.showValidationErrors = false;
    this.resetForm();

    this.alertService.showMessage('Cancelled', 'Operation cancelled by patient', MessageSeverity.default);
    this.alertService.resetStickyMessage();

    if (!this.isGeneralEditor) {
      this.isEditMode = false;
    }

    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }


  close() {
    this.patientEdit = this.patient = new PatientEdit();
    this.showValidationErrors = false;
    this.resetForm();
    this.isEditMode = false;

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }



  private refreshLoggedInPatient() {
    this.patientService.refreshLoggedInPatient()
      .subscribe(patient => {
        this.loadCurrentPatientData();
      },
        error => {
          this.alertService.resetStickyMessage();
          this.alertService.showStickyMessage('Refresh failed', 'An error occured whilst refreshing logged in patient information from the server', MessageSeverity.error, error);
        });
  }


  changePassword() {
    this.isChangePassword = true;
  }





  resetForm(replace = false) {
    this.isChangePassword = false;

    if (!replace) {
      this.form.reset();
    } else {
      this.formResetToggle = false;

      setTimeout(() => {
        this.formResetToggle = true;
      });
    }
  }


  newPatient(allRoles: Role[]) {
    this.isGeneralEditor = true;
    this.isNewPatient = true;

    this.allRoles = [...allRoles];
    this.patient = this.patientEdit = new PatientEdit();
   
    this.edit();

    return this.patientEdit;
  }

  editPatient(patient: Patient, allRoles: Role[]) {
    if (patient) {
      this.isGeneralEditor = true;
      this.isNewPatient = false;

      this.setRoles(patient, allRoles);
      this.patient = new Patient();
      this.patientEdit = new PatientEdit();
      Object.assign(this.patient, patient);
      Object.assign(this.patientEdit, patient);
      this.edit();

      return this.patientEdit;
    } else {
      return this.newPatient(allRoles);
    }
  }


  displayPatient(patient: Patient, allRoles?: Role[]) {

    this.patient = new Patient();
    Object.assign(this.patient, patient);
    this.setRoles(patient, allRoles);

    this.isEditMode = false;
  }



  private setRoles(patient: Patient, allRoles?: Role[]) {

    // this.allRoles = allRoles ? [...allRoles] : [];

    // if (patient.roles) {
    //   for (const ur of patient.roles) {
    //     if (!this.allRoles.some(r => r.name === ur)) {
    //       this.allRoles.unshift(new Role(ur));
    //     }
    //   }
    // }
  }



  get canViewAllRoles() {
    return this.patientService.userHasPermission(Permission.viewRolesPermission);
  }

  get canAssignRoles() {
    return this.patientService.userHasPermission(Permission.assignRolesPermission);
  }
}
