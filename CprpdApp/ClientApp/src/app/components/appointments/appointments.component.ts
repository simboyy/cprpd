// =============================
// Email: smkorera@gmail.com

// =============================

import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animations';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss'],
  animations: [fadeInOut]
})
export class AppointmentsComponent {
}
