

// =============================
// Email: smkorera@gmail.com

// =============================

import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective, ModalOptions } from 'ngx-bootstrap/modal';
import { fadeInOut } from '../../services/animations';

import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from '../../services/app-translation.service';
import { PatientService } from '../../services/patient.service';
import { Utilities } from '../../services/utilities';
import { Patient } from '../../models/patient-model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { PatientEdit } from '../../models/patient-edit.model';
import { PatientInfoComponent } from './../controls/patient-info.component';


@Component({
  selector: 'app-Patients',
  templateUrl: './Patients.component.html',
  styleUrls: ['./Patients.component.scss'],
  animations: [fadeInOut]
})
export class PatientsComponent implements OnInit, AfterViewInit {
  columns: any[] = [];
  rows: Patient[] = [];
  rowsCache: Patient[] = [];
  editedPatient: PatientEdit;
  sourcePatient: PatientEdit;
  editingPatientName: { name: string };
  loadingIndicator: boolean;

  allRoles: Role[] = [];


  @ViewChild('indexTemplate', { static: true })
  indexTemplate: TemplateRef<any>;

  @ViewChild('userNameTemplate', { static: true })
  userNameTemplate: TemplateRef<any>;

  @ViewChild('rolesTemplate', { static: true })
  rolesTemplate: TemplateRef<any>;

  @ViewChild('actionsTemplate', { static: true })
  actionsTemplate: TemplateRef<any>;

  @ViewChild('editorModal', { static: true })
  editorModal: ModalDirective;

  @ViewChild('patientEditor', { static: true })
  patientEditor: PatientInfoComponent;

  constructor(private alertService: AlertService, private translationService: AppTranslationService, private patientService: PatientService) {
  }


  ngOnInit() {

    const gT = (key: string) => this.translationService.getTranslation(key);

    this.columns = [
      { prop: 'index', name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
      { prop: 'fullName', name: gT('patients.management.FullName'), width: 120 },
      { prop: 'phoneNumber', name: gT('patients.management.PhoneNumber'), width: 100 },
      { prop: 'emailAddress', name: gT('patients.management.EmailAddress'), width: 140 },
      { prop: 'gender', name: gT('patients.management.Gender'), width: 140 },
      { prop: 'dateOfBirth', name: gT('patients.management.DateOfBirth'), width: 140 },
      // { prop: 'roles', name: gT('patients.management.Roles'), width: 120, cellTemplate: this.rolesTemplate },
      { prop: 'deadLineForSupport', name: gT('patients.management.DeadLineForSupport'), width: 100 }
    ];

    if (this.canManagePatients) {
      this.columns.push({ name: '', width: 160, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });
    }

    this.loadData();
  }


  ngAfterViewInit() {

    this.patientEditor.changesSavedCallback = () => {
      this.addNewPatientToList();
      this.editorModal.hide();
    };

    this.patientEditor.changesCancelledCallback = () => {
      this.editedPatient = null;
      this.sourcePatient = null;
      this.editorModal.hide();
    };
  }


  addNewPatientToList() {
    if (this.sourcePatient) {
      Object.assign(this.sourcePatient, this.editedPatient);

      let sourceIndex = this.rowsCache.indexOf(this.sourcePatient, 0);
      if (sourceIndex > -1) {
        Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);
      }

      sourceIndex = this.rows.indexOf(this.sourcePatient, 0);
      if (sourceIndex > -1) {
        Utilities.moveArrayItem(this.rows, sourceIndex, 0);
      }

      this.editedPatient = null;
      this.sourcePatient = null;
    } else {
      const user = new Patient();
      Object.assign(user, this.editedPatient);
      this.editedPatient = null;

      let maxIndex = 0;
      for (const u of this.rowsCache) {
        if ((u as any).index > maxIndex) {
          maxIndex = (u as any).index;
        }
      }

      (user as any).index = maxIndex + 1;

      this.rowsCache.splice(0, 0, user);
      this.rows.splice(0, 0, user);
      this.rows = [...this.rows];
    }
  }


  loadData() {
    this.alertService.startLoadingMessage();
    this.loadingIndicator = true;

    if (this.canViewRoles) {
      // this.patientService.getPatientsAndRoles().subscribe(results => this.onDataLoadSuccessful(results[0], results[1]), error => this.onDataLoadFailed(error));
      this.patientService.getPatients().subscribe(patients => this.onDataLoadSuccessful(patients, this.patientService.currentPatient.roles.map(x => new Role(x))), error => this.onDataLoadFailed(error));
    } else {
      this.patientService.getPatients().subscribe(patients => this.onDataLoadSuccessful(patients, this.patientService.currentPatient.roles.map(x => new Role(x))), error => this.onDataLoadFailed(error));
    }
  }


  onDataLoadSuccessful(patients: Patient[], roles: Role[]) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    patients.forEach((user, index) => {
      (user as any).index = index + 1;
    });

    this.rowsCache = [...patients];
    this.rows = patients;

    this.allRoles = roles;
  }


  onDataLoadFailed(error: any) {
    this.alertService.stopLoadingMessage();
    this.loadingIndicator = false;

    this.alertService.showStickyMessage('Load Error', `Unable to retrieve patients from the server.\r\nErrors: "${Utilities.getHttpResponseMessages(error)}"`,
      MessageSeverity.error, error);
  }


  onSearchChanged(value: string) {
    this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.fullName, r.emailAddress, r.phoneNumber));
  }

  onEditorModalHidden() {
    this.editingPatientName = null;
    this.patientEditor.resetForm(true);
  }


  newPatient() {
    this.editingPatientName = null;
    this.sourcePatient = null;
    this.editedPatient = this.patientEditor.newPatient(this.allRoles);
    const config :ModalOptions = { class : 'modal-lg'}
    this.editorModal.show();
  }


  editPatient(row: PatientEdit) {
    this.editingPatientName = { name: row.fullName };
    this.sourcePatient = row;
    this.editedPatient = this.patientEditor.editPatient(row, this.allRoles);
    this.editorModal.show();
  }


  deletePatient(row: PatientEdit) {
    this.alertService.showDialog('Are you sure you want to delete \"' + row.fullName + '\"?', DialogType.confirm, () => this.deletePatientHelper(row));
  }


  deletePatientHelper(row: PatientEdit) {

    this.alertService.startLoadingMessage('Deleting...');
    this.loadingIndicator = true;

    this.patientService.deletePatient(row)
      .subscribe(results => {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.rowsCache = this.rowsCache.filter(item => item !== row);
        this.rows = this.rows.filter(item => item !== row);
      },
        error => {
          this.alertService.stopLoadingMessage();
          this.loadingIndicator = false;

          this.alertService.showStickyMessage('Delete Error', `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessages(error)}"`,
            MessageSeverity.error, error);
        });
  }



  get canAssignRoles() {
    return this.patientService.userHasPermission(Permission.assignRolesPermission);
  }

  get canViewRoles() {
    return this.patientService.userHasPermission(Permission.viewRolesPermission);
  }

  get canManagePatients() {
    return this.patientService.userHasPermission(Permission.manageUsersPermission);
  }
}

