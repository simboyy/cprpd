// =============================
// Email: smkorera@gmail.com

// =============================

export class Patient {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(
        id?: string, fullName?: string, phoneNumber?: string, emailAddress?: string, placeOfResidence?: string, disabilityGroup?: string,
        disability?: string, gender?: string, dateOfBirth?: string, hearingSpeechDiction?: string, pcKnowledge?: string, training?: string,
        education?: string, motivation?: string, desiredWorkingConditions?: string, desiredVacancy?: string, employmentStatus?: string, employmentDescription?: string,
        formOfEmployment?: string, employmentDate?: string, availability?: string, deadLineForSupport?: string, additionalInfo?: string, responsiblePerson?: string,
        disabilityGroups?: string[]) {

        this.id = id;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.placeOfResidence = placeOfResidence;
        this.disabilityGroup = disabilityGroup;
        this.disability = disability;
        this.gender= gender;
        this.dateOfBirth = dateOfBirth;
        this.hearingSpeechDiction =hearingSpeechDiction;
        this.pcKnowledge = pcKnowledge;
        this.training = training;
        this.education = education;
        this.motivation =motivation;
        this.desiredWorkConditions = desiredWorkingConditions;
        this.desiredVacancy = desiredVacancy;
        this.employmentStatus =  employmentStatus;
        this.employmentDescription = employmentDescription;
        this.formOfEmployment = formOfEmployment;
        this.employmentDate = employmentDate;
        this.availability = availability;
        this.deadLineForSupport = deadLineForSupport;
        this.additionalInfo = additionalInfo;
        this.responsiblePerson = responsiblePerson
        this.disabilityGroups = disabilityGroups;
    }



    public id: string;
    public fullName: string;
    public phoneNumber: string;
    public emailAddress: string;
    public placeOfResidence: string;
    public disabilityGroup: string;
    public disability: string;
    public gender: string;
    public dateOfBirth: string;
    public hearingSpeechDiction: string;
    public pcKnowledge: string;
    public training: string;
    public education: string;
    public motivation: string;
    public desiredWorkConditions: string;
    public desiredVacancy: string;
    public employmentStatus: string;
    public employmentDescription: string;
    public formOfEmployment: string;
    public employmentDate: string;
    public availability: string;
    public deadLineForSupport: string;
    public additionalInfo: string;
    public responsiblePerson: string;
    public disabilityGroups: string[];
}
