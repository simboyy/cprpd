// =============================
// Email: smkorera@gmail.com

// =============================

export enum Gender {
    None,
    Female,
    Male
}
