﻿// =============================
// Email: smkorera@gmail.com

// =============================

using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CprpdApp.ViewModels
{
    public class PatientViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string PlaceOfResidence { get; set; }
        public int DisabilityGroup { get; set; }
        public string Disability { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string HearingSpeechDiction { get; set; }
        public string Education { get; set; }
        public string Training { get; set; }
        public string Motivation { get; set; }
        public string DesiredWorkingConditions { get; set; }
        public string DesiredVacancy { get; set; }
        public string EmploymentStatus { get; set; }
        public string FormOfEmployment { get; set; }
        public string EmploymentDate { get; set; }
        public string Availability { get; set; }
        public DateTime DeadLineForSupport { get; set; }
        public string AdditionalInfo { get; set; }
        public string ResponsiblePerson { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public ICollection<RehabilitationViewModel> Rehabilitations { get; set; }
    }




    public class PatientViewModelValidator : AbstractValidator<PatientViewModel>
    {
        public PatientViewModelValidator()
        {
            RuleFor(register => register.FullName).NotEmpty().WithMessage("Patient name cannot be empty");
            RuleFor(register => register.Gender).NotEmpty().WithMessage("Gender cannot be empty");
        }
    }
}
