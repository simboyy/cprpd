﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Linq;


namespace CprpdApp.ViewModels
{
    public class QuestionaireViewModel
    {
        public int Id { get; set; }
        public decimal Discount { get; set; }
        public string Comments { get; set; }
    }
}
