﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Linq;


namespace CprpdApp.ViewModels
{
    public class RehabilitationViewModel
    {
        public int Id { get; set; }
        public decimal Discount { get; set; }
        public string Comments { get; set; }
    }
}
