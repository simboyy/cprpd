﻿// =============================
// Email: smkorera@gmail.com

// =============================

using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CprpdApp.ViewModels
{
    public class JobViewModel
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }
        public string Company { get; set; }

        public string Vacancy { get; set; }

        public int NumberOfUnits { get; set; }

        public decimal Salary { get; set; }

        public string JobRequiremets { get; set; }

        public string EmployerPhone { get; set; }

        public string EmployerEmail { get; set; }

        public string Industry { get; set; }
        public string Notes { get; set; }
        public string EmployerAddress { get; set; }

        public ICollection<JobViewModel> Jobs { get; set; }
    }




    public class JobViewModelValidator : AbstractValidator<JobViewModel>
    {
        public JobViewModelValidator()
        {
            RuleFor(register => register.Company).NotEmpty().WithMessage("Company name cannot be empty");
            RuleFor(register => register.Vacancy).NotEmpty().WithMessage("Vacancy cannot be empty");
            RuleFor(register => register.NumberOfUnits).NotEmpty().WithMessage("Number Of Units cannot be empty");
        }
    }
}
