﻿// =============================
// Email: smkorera@gmail.com

// =============================

using DAL.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CprpdApp.ViewModels
{
    public class JobApplicationViewModel
    {
        public int Id { get; set; }


        public int PatientId { get; set; }
        public Patient Patient { get; set; }

        public int ApplicationId { get; set; }
        public JobApplicationViewModel Application { get; set; }

        public string Status { get; set; }

        public string Notes { get; set; }

        public ICollection<RehabilitationViewModel> Rehabilitations { get; set; }
    }




    public class JobApplicationViewModelValidator : AbstractValidator<JobApplicationViewModel>
    {
        public JobApplicationViewModelValidator()
        {
         
        }
    }
}
