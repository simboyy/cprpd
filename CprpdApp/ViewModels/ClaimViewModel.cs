﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Linq;

namespace CprpdApp.ViewModels
{
    public class ClaimViewModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
