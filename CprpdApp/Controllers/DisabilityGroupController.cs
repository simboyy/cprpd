﻿// =============================
// Email: smkorera@gmail.com

// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DAL;
using CprpdApp.ViewModels;
using AutoMapper;
using Microsoft.Extensions.Logging;
using CprpdApp.Helpers;

namespace CprpdApp.Controllers
{
    [Route("api/[controller]")]
    public class DisabilityGroupController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;


        public DisabilityGroupController(IMapper mapper, IUnitOfWork unitOfWork, ILogger<DisabilityGroupController> logger, IEmailSender emailSender)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _logger = logger;
            _emailSender = emailSender;
        }



        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            var allDisabilityGroups = _unitOfWork.DisabilityGroups.GetAll();
            return Ok(_mapper.Map<IEnumerable<DisabilityGroupViewModel>>(allDisabilityGroups));
        }



        [HttpGet("throw")]
        public IEnumerable<DisabilityGroupViewModel> Throw()
        {
            throw new InvalidOperationException("This is a test exception: " + DateTime.Now);
        }



        [HttpGet("email")]
        public async Task<string> Email()
        {
            string recepientName = "CprpdApp Tester"; //         <===== Put the recepient's name here
            string recepientEmail = "test@cbrppd.com"; //   <===== Put the recepient's email here

            string message = EmailTemplates.GetTestEmail(recepientName, DateTime.UtcNow);

            (bool success, string errorMsg) = await _emailSender.SendEmailAsync(recepientName, recepientEmail, "Test Email from CprpdApp", message);

            if (success)
                return "Success";

            return "Error: " + errorMsg;
        }



        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value: " + id;
        }



        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }



        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }



        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
